﻿choco install git -y
choco install jdk8 -y
choco install sourcetree -y
choco install dotnet3.5 -y
choco install dotnet4.5 -y
choco install visualstudiocode -y
choco install visualstudio2015community -y
choco install sql-server-management-studio -y
choco install ssdt15 -y
choco install nunit -y
choco install specflow -y

Read-Host "Press ENTER to close..."